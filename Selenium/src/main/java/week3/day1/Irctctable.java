package week3.day1;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Irctctable {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://erail.in/");
		driver.manage().window().maximize();
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MDU",Keys.TAB);
		WebElement checkele = driver.findElementById("chkSelectDateOnly");
		if(checkele.isSelected())
		{
			checkele.click();
		}
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("SBC",Keys.TAB);
		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> allrow = table.findElements(By.tagName("tr"));
		System.out.println("Allrows" +allrow.size());
		for (int i = 0; i < allrow.size(); i++) {
			List<WebElement> allcol = allrow.get(i).findElements(By.tagName("td"));
			String text = allcol.get(1).getText();
			System.out.println(text);
			
		}
	}
}
