package week3.day1;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Irctchw {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
		driver.findElementById("userRegistrationForm:userName").sendKeys("Geetha");
		driver.findElementById("userRegistrationForm:password").sendKeys("Password");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("Password");
		WebElement secq=driver.findElementById("userRegistrationForm:securityQ");
		Select ques=new Select(secq);
		ques.selectByVisibleText("What is your pet name?");
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Jittu");
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Geetha");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("Kalaiselvan");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Karthik");
		driver.findElementById("userRegistrationForm:gender:1").click();
		driver.findElementById("userRegistrationForm:maritalStatus:0").click();
		WebElement day=driver.findElementByName("userRegistrationForm:dobDay");
		Select dd=new Select(day);
		dd.selectByValue("23");
		WebElement mon=driver.findElementByName("userRegistrationForm:dobMonth");
		Select mm=new Select(mon);
		mm.selectByVisibleText("MAR");
		WebElement year=driver.findElementByName("userRegistrationForm:dateOfBirth");
		Select yr=new Select(year);
		yr.selectByValue("1988");
		WebElement occ=driver.findElementByName("userRegistrationForm:occupation");
		Select occ1=new Select(occ);
		occ1.selectByVisibleText("Private");
		WebElement con=driver.findElementByName("userRegistrationForm:countries");
		Select con1=new Select(con);
		con1.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:email").sendKeys("geetakalaiselvan@gmail.com");
		//driver.findElementById("userRegistrationForm:isdCode").sendKeys("+91");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("9940063076");
		WebElement nat=driver.findElementById("userRegistrationForm:nationalityId");
		Select nat1=new Select(nat);
		nat1.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:address").sendKeys("Mugappair west");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600037",Keys.TAB);
		Thread.sleep(10000);
		WebElement town=driver.findElementById("userRegistrationForm:cityName");
		Select tw=new Select(town);
		tw.selectByVisibleText("Tiruvallur");
		WebElement postoff=driver.findElementById("userRegistrationForm:postofficeName");
		Select po=new Select(postoff);
		po.selectByIndex(1);
		driver.findElementById("userRegistrationForm:landline").sendKeys("23456788");
		driver.findElementById("userRegistrationForm:resAndOff:1").click();
		Thread.sleep(10000);
		driver.findElementByLinkText("Submit Registration Form").click();
		
		
		
	}
	
	

}
