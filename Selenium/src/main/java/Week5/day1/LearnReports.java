package Week5.day1;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReports {
	
	@Test

	public void name() throws IOException
	{
		ExtentHtmlReporter html=new ExtentHtmlReporter("./Reports/result.html");
		html.setAppendExisting(false);
		ExtentReports extent=new ExtentReports();
		extent.attachReporter(html);
		
		ExtentTest logger=extent.createTest("Createlead", "create anew lead");
		logger.assignAuthor("Geetha");
		logger.assignCategory("Regression");
		logger.log(Status.PASS, "Demosales Manager Entered Sucessucessfully", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());
		
		logger.log(Status.PASS, "crmsfa Entered Sucessucessfully", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img2.png").build());

		logger.log(Status.PASS, "login Sucessucessfully", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img3.png").build());
		
		extent.flush();
		
	}
	
}
