package wdMethods;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import com.beust.jcommander.Parameter;


public class ProjectMethods extends SeMethods{
	
	@Parameters({"url","username","password"})
	@BeforeMethod
	public void login(String url,String username,String  password) throws InterruptedException {
		beforeMethod();
		startApp("chrome", url);
		
		//startApp("chrome", "https://www.zoomcar.com/chennai/");
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement loctext = locateElement("LinkText", "CRM/SFA");
		click(loctext);

	}
	
	
	@AfterMethod(groups= {"Any"})
	public void closeapp()
	{
	 closeBrowser();
	}
	@BeforeSuite(groups= {"Any"})
	public void beforeSuite() {
	startResult();
	}
	
	@AfterSuite(groups= {"Any"})
	public void afterSuite() {
		endResult();
	}
	
}


