package week2.day1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Strhw2 {

	public static void main(String[] args) {
		String txt="1c2d3gb";
		String pat="[a-z0-9]";
		Pattern p=Pattern.compile(pat);
		Matcher matcher=p.matcher(txt);
		System.out.println(matcher.matches());
	}

}
