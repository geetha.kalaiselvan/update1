package week2.day1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Strhw {

	public static void main(String[] args) {
		String txt="geetha@gmail.com";
		String pat="[a-zA-Z0-9]{0,100}@[a-zA-Z]{5}.[a-zA-Z]{2,3}";
		Pattern p=Pattern.compile(pat);
		Matcher matcher=p.matcher(txt);
		System.out.println(matcher.matches());
		
	}

}
