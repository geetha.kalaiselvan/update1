package week2.day2;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class One {

	public static void main(String[] args) {
		String st="I am getting better every day";
		String re = st.replace(" ", "");
		char[] s1=re.toCharArray();
		System.out.println(s1);
		Map<Character,Integer> s2=new HashMap<Character,Integer>();
		for (char c : s1) {
			if(s2.containsKey(c))
			{
				s2.put(c,(Integer)s2.get(c) +1);
			}
			else
			{
				s2.put(c,1);
		}
		}
		System.out.println(s2);
	   }
		    
		
}


