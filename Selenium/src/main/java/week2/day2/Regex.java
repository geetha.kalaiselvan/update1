package week2.day2;

import java.util.regex.*;
//import java.util.regex.Pattern;

public class Regex {

    public static void main(String[] args) {
        
        /*Pattern p = Pattern.compile(".s");//. represents single character  
        Matcher m = p.matcher("as");  
        boolean b = m.matches(); */  
    
    
    System.out.println(Pattern.matches("[amn]", "a")); //output - true
        
    System.out.println(Pattern.matches("[^abc]", "z"));//Any character except a, b, or c (negation)
                
    System.out.println(Pattern.matches("[a-zA-Z]", "z"));//a through z or A through Z, inclusive (range)
    System.out.println(Pattern.matches("[a-d[m-p]]", "o"));// any of the chars from a to d and m to p
    System.out.println(Pattern.matches("[a-z&&[def]]", "d"));//only chars intersection of both are valid.i.e.def
    System.out.println(Pattern.matches("[a-z&&[^bc]]", "a"));//a through z, except for b and c: [ad-z] (subtraction)
    System.out.println(Pattern.matches("[a-z&&[^m-p]]", "z"));    //a through z, and not m through p: [a-lq-z] (subtraction)
    
    System.out.println("predefined character classes");
    System.out.println(Pattern.matches(".", "a"));    //Any character (may or may not match line terminators)
    System.out.println(Pattern.matches("\\d", "1"));    //digit
    System.out.println(Pattern.matches("\\D", "a"));    //non-digit
        System.out.println(Pattern.matches("\\s", " "));    //A whitespace character:[ \t\n\x0B\f\r]
    System.out.println(Pattern.matches("\\S", "a"));    //A non-whitespace character:[^\s]
    System.out.println(Pattern.matches("\\w", "a"));    //A word character
    System.out.println(Pattern.matches("\\W", "!"));    //A non-word character
                        
    
    System.out.println("Regex Quantifiers");
    System.out.println(Pattern.matches("[amn]?", "a"));//true (a or m or n comes one time)  
    System.out.println(Pattern.matches("[amn]?", "aaa"));//false (a comes more than one time)  
    System.out.println(Pattern.matches("[amn]?", "aammmnn"));//false (a m and n comes more than one time)  
    System.out.println(Pattern.matches("[amn]?", "aazzta"));//false (a comes more than one time)  
    System.out.println(Pattern.matches("[amn]?", "am"));//false (a or m or n must come one time)  
    
    System.out.println(Pattern.matches("[amn]+", "a"));//true (a or m or n once or more times)  
    System.out.println(Pattern.matches("[amn]+", "aaa"));//true (a comes more than one time)  
    System.out.println(Pattern.matches("[amn]+", "aammmnn"));//true (a or m or n comes more than once)  
    System.out.println(Pattern.matches("[amn]+", "aazzta"));//false (z and t are not matching pattern)  
    
    System.out.println(Pattern.matches("X*", "XXX"));    //X occurs zero or more times
    System.out.println(Pattern.matches("X*", ""));    //X occurs zero or more times
    System.out.println(Pattern.matches("X+", "XXXX"));    //X occurs one or more times
    System.out.println(Pattern.matches("X{5}", "XXXXX")); //X occurs exactly n times
    System.out.println(Pattern.matches("X{1,}", "X")); //X occurs atleast n times -- True
    System.out.println(Pattern.matches("X{1,}", "XY")); //X occurs atleast n times -- False
    
    System.out.println(Pattern.matches("X{1,3}", "XX")); //X occurs atleast n times but not more than m times
    
    
    
    
    }}