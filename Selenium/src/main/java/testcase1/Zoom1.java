package testcase1;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class Zoom1 extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "Ex1ZoonCarFindPrice";
		testDesc = "Find Maximum priced Car model in Zoor Car";
		author = "Geetha";
		category = "smoke";
	}

	@Test
	public void car()
	{		
		WebElement loc1 = locateElement("LinkText", "Start your wonderful journey");
		click(loc1);

		WebElement loc2 = locateElement("xpath","//div[contains(text(),'Nelson Manickam Road')]");
		click(loc2);

		WebElement loc3 = locateElement("xpath","//button[contains(text(),Next)]");
		click(loc3);

		Date date = new Date();
		DateFormat sdf = new SimpleDateFormat("dd"); 
		String today = sdf.format(date);
		int tomorrow = Integer.parseInt(today)+1;

		WebElement loc4 = locateElement("xpath","(//div[contains(text(),'"+tomorrow+ "')])");
		click(loc4);

		WebElement loc5 = locateElement("xpath","//button[text()='Next']");
		click(loc5);

		WebElement loc6 = locateElement("xpath","//div[contains(text(),'15')]");
		click(loc6);
		WebElement loc7 = locateElement("xpath","//button[text()='Done']");
		click(loc7);
				
		//div[@class='price']
		List<WebElement> priceElement = driver.findElementsByXPath("//div[@class='price']");
		List<String> price=new ArrayList<>();
		for (WebElement Eachelement : priceElement) {
			price.add(Eachelement.getText().trim());
		}
		List<Integer> rsymbol=new ArrayList<>();
		for (String eachprice : price) {
			rsymbol.add(Integer.parseInt(eachprice.replaceAll("[^0-9]", "")));
		}
		Collections.sort(rsymbol);
				
		Integer integer = rsymbol.get(rsymbol.size()-1);
		System.out.println(integer);
		
		//System.out.println("//div[@class='price' and contains(text(),'"+integer+"') ]");
		//System.out.println("//div[@class='price' and contains(text(),'"+integer+ "') ]/../../preceding-sibling::div[1]/h3");
		WebElement loc8 = locateElement("xpath","//div[@class='price' and contains(text(),'"+integer+"') ]/preceding::h3[1]");
		System.out.println(loc8.getText());
		////div[@class='price' and contains(text(),'320') ]/../../preceding::h3
		////div[@class='price' and contains(text(),'320') ]/preceding::h3
		//div[@class='price' and contains(text(),'320') ]/preceding::button
		WebElement loc9 = locateElement("xpath","//div[@class='price' and contains(text(),'"+integer+"') ]/preceding::button");
		click(loc9);
	























		/*int max=0;
	for (String maxprice : rsymbol) {
		if(Integer.parseInt(maxprice)>0)
		{
			max=Integer.parseInt(maxprice);
		}
		System.out.println(max);
	}

	List<WebElement> nameele = driver.findElementsByXPath("//div[@class=\"details\"]/h3");
	List<String> name=new ArrayList<>();
	for (WebElement EachelementName : nameele) {
		nameele.add(getText());
	}


	}


	}
	}
		 */
	}






}




