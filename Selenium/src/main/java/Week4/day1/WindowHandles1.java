package Week4.day1;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandles1 {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
		
		Set<String> wind = driver.getWindowHandles();
		List<String> str=new ArrayList<>();
		str.addAll(wind);
		driver.switchTo().window(str.get(1));
		driver.findElementByName("firstName").sendKeys("Geetha");
		String text = driver.findElementByName("firstName").getText();
		System.out.println("First Name-->" +text);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		
		driver.close();
		
		/*driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
	
		Set<String> wind1 = driver.getWindowHandles();
		List<String> str1=new ArrayList<>();
		str.addAll(wind1);
		driver.switchTo().window(str1.get(1));
		driver.findElementByName("firstName").sendKeys("Binu");
		String text1 = driver.findElementByName("firstName").getText();
		System.out.println("First Name-->" +text1);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		
		driver.close();*/
		
		/*Set<String> wind1 = driver.getWindowHandles();
		List<String> str1=new ArrayList<>();
		str.addAll(wind1);
		driver.switchTo().window(str1.get(0));
		*/
		
		
		
		
		
			
		
		
		
	}

}
