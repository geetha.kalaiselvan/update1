package Week4.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Framelearn {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().window().maximize();
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
		driver.switchTo().alert().sendKeys("Geetha");
		driver.switchTo().alert().accept();
	//	WebElement linkele = driver.findElementByLinkText("//p[text()='Hello Geetha! How are you today?']");
		String text = driver.findElementById("demo").getText();
		if(text.contains("Geetha"))
		{
			System.out.println("Displayed");
		}
		
		
	}

}
