package Week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.junit.rules.Timeout;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandle {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/loginHome.jsf");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByLinkText("AGENT LOGIN").click();
		
		
		driver.findElementByLinkText("Contact Us").click();
		
		Set<String> win = driver.getWindowHandles();
		List<String> st=new ArrayList<>();
		st.addAll(win);
		WebDriver secwin = driver.switchTo().window(st.get(1));
		System.out.println(secwin);
		
		File src=driver.getScreenshotAs(OutputType.FILE);
		File obj=new File("./snaps/img1.jpeg");
		FileUtils.copyFile(src, obj);
				
		Set<String> win1 = driver.getWindowHandles();
		List<String> st1=new ArrayList<>();
		st.addAll(win1);
		//driver.close();
		WebDriver secwin1 = driver.switchTo().window(st.get(2));
		secwin1.close();
		//System.out.println(secwin1);
		
		
	
		
		
	}

}
