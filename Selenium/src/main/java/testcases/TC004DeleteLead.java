package testcases;


	import org.openqa.selenium.WebElement;
	import org.testng.annotations.Test;

	public class TC004DeleteLead extends CreateLead {
		@Test
		
		public void deleteLead() {
			WebElement del = locateElement("LinkText", "Delete");
			click(del);
			System.out.println("Lead record is deleted successfully");
		}
	}


