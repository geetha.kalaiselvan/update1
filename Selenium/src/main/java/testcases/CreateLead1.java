package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class CreateLead1 {
	public class CreateLead extends ProjectMethods{
		
		@BeforeTest(groups= {"Any"})
		public void setData() {
			testCaseName = "Edit";
			testDesc = "Edit lead";
			author = "Geetha";
			category = "smoke";
		}
		
		@Test(groups= {"Regression"})
		public void createlead() throws InterruptedException
		{
					WebElement loctext1 = locateElement("LinkText", "Create Lead");
			click(loctext1);
			WebElement loctext2 = locateElement("id", "createLeadForm_companyName");
			type(loctext2, "Infosys");
			/*WebElement pathx = locateElement("xpath","//img[@alt='Lookup']");
			click(pathx);
			switchToWindow(1);
			WebElement loctex6 = locateElement("name","id");
			type(loctex6,"accountlimit100");
			WebElement loc1 = locateElement("xpath", "(//button[@class='x-btn-text'])[1]");
			Thread.sleep(3000);
			click(loc1);
			WebElement loc2 = locateElement("xpath", "//a[text()='accountlimit100']");
			click(loc2);
			switchToWindow(0);*/
			
			WebElement loctext3 = locateElement("xpath", "//input[@id='createLeadForm_firstName']");
			type(loctext3, "Geetha");
			WebElement loctext4 = locateElement("id", "createLeadForm_lastName");
			type(loctext4, "Kalaiselvan");
			WebElement loctext5 = locateElement("id", "createLeadForm_industryEnumId");
			selectDropDownUsingText(loctext5,"Health Care");
			WebElement loctext6 = locateElement("name", "submitButton");
			click(loctext6);
			/*driver.switchTo().window("center-content-column");
			WebElement loctext7=locateElement("xath","//a[text()='Duplicate Lead']");
			click(loctext7);*/
			
			
			
			
			
			
			
			
			
			
			
		
		}
	}

}
