package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class TC003DuplicateLead extends CreateLead {
	@Test
	public void duplicateLead() throws InterruptedException {
//		createLead();
		WebElement dupLead = locateElement("xpath", "//a[text()='Duplicate Lead']");
		click(dupLead);
		WebElement submitDupLead = locateElement("xpath", "//input[@value='Create Lead']");
		click(submitDupLead);
	}
}
