package testcases;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import testcase1.LearnExcel;
import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class CreateLead extends ProjectMethods{
	
	@BeforeTest(groups= {"Any"})
	public void setData() {
		testCaseName = "Edit";
		testDesc = "Edit lead";
		author = "Geetha";
		category = "smoke";
	}
	@DataProvider(name="qa" ,indices= {2})
	public Object[][] fecthData() throws IOException{
		
		Object[][] data=LearnExcel.readexcel();
		return data;
		
		
		/*data[0][0]="Testleaf";
		data[0][1]="Geetha";
		data[0][2]="K";
		data[0][3]="Geetha@testleaf.com";
		
		data[1][0]="Testleaf";
		data[1][1]="Geetha";
		data[1][2]="K";
		data[1][3]="Geetha11@testleaf.com";
		return data;*/
		
	}
		
		
	@Test(dataProvider="qa")
	public void createlead(String commapnyname,String FirstName,String lastname) throws InterruptedException
	{
				WebElement loctext1 = locateElement("LinkText", "Create Lead");
		click(loctext1);
		WebElement loctext2 = locateElement("id", "createLeadForm_companyName");
		type(loctext2, commapnyname);
		/*WebElement pathx = locateElement("xpath","//img[@alt='Lookup']");
		click(pathx);
		switchToWindow(1);
		WebElement loctex6 = locateElement("name","id");
		type(loctex6,"accountlimit100");
		WebElement loc1 = locateElement("xpath", "(//button[@class='x-btn-text'])[1]");
		Thread.sleep(3000);
		click(loc1);
		WebElement loc2 = locateElement("xpath", "//a[text()='accountlimit100']");
		click(loc2);
		switchToWindow(0);*/
		
		WebElement loctext3 = locateElement("xpath", "//input[@id='createLeadForm_firstName']");
		type(loctext3, FirstName);
		WebElement loctext4 = locateElement("id", "createLeadForm_lastName");
		type(loctext4, lastname);
		
		/*WebElement loctext5 = locateElement("id", "createLeadForm_industryEnumId");
		selectDropDownUsingText(loctext5,"Health Care");*/
		WebElement loctext6 = locateElement("name", "submitButton");
		click(loctext6);
		/*driver.switchTo().window("center-content-column");
		WebElement loctext7=locateElement("xath","//a[text()='Duplicate Lead']");
		click(loctext7);*/
		
		
		
		
		
		
		
		
		
		
		
	
	}
}
