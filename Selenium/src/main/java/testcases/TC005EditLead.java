package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC005EditLead extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName = "Edit";
		testDesc = "Edit lead";
		author = "Geetha";
		category = "smoke";
	}

	@Test//(dependsOnMethods= {"testcases.CreateLead.createlead"})
	public void editLead() {
		WebElement loc0 = locateElement("xpath","//a[text()='Leads']");
		click(loc0);
		WebElement loc1 = locateElement("xpath","//a[text()='Find Leads']");
		click(loc1);
		WebElement loc2 = locateElement("xpath","(//input[@name=\"firstName\"])[3]");
		type(loc2, "Geetha");
		
		WebElement loc3 = locateElement("xpath","//button[text()='Find Leads']");
		click(loc3);
		
		WebElement loc4 = locateElement("xpath","(//a[@class=\"linktext\"])[1]");
		click(loc4);
	
		WebElement editClick = locateElement("xpath", "//a[text()='Edit']");
		click(editClick);
		WebElement editLeadScreen = locateElement("xpath", "//div[text()='Edit Lead']");
		verifyExactText(editLeadScreen, "Edit Lead");
		
		WebElement descText = locateElement("updateLeadForm_description");
		type(descText, "This is to edit already created lead");
		
		WebElement updateLead = locateElement("xpath", "//input[@value='Update']");
		click(updateLead);
		
		WebElement viewLeadScreen = locateElement("xpath", "//div[text()='View Lead']");
		verifyExactText(viewLeadScreen, "View Lead");
		
	}
}
